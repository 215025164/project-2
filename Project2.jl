### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 4970a490-d1ca-11eb-09ea-d1f964b1dbe6
using Flux

# ╔═╡ 99525720-3c2f-4720-afd6-8f54e882b441
using Flux: Data.DataLoader

# ╔═╡ b0c701d6-9c81-4eaa-aec5-804d21748f36
using Flux: onehotbatch, onecold, crossentropy

# ╔═╡ 91df2c6e-87f9-4e1f-b3b6-592ff6d8dc4a
using Flux: @epochs

# ╔═╡ d0bce8f3-1383-4b4e-b4f1-71c90a9344a3
using Statistics

# ╔═╡ 8cd5fa85-1f83-49b7-8989-efc6b3b30900
using MLDatasets

# ╔═╡ 126acf4f-4039-417a-85df-0f4c7052083b
md"## Loading Data"

# ╔═╡ 0b5212f1-55fd-4e80-b930-ed9e2407951e
begin
	x_train, y_train = MLDatasets.MNIST.traindata()
	x_train = Flux.unsqueeze(x_train, 3)
	y_train = onehotbatch(y_train, 0:9)
end

# ╔═╡ 0a0ffcc5-4bfa-4669-9590-df7bb5edf0a8
begin
	x_valid, y_valid = MLDatasets.MNIST.testdata()
	x_valid = Flux.unsqueeze(x_valid, 3)
	y_valid = onehotbatch(y_valid, 0:9)
end

# ╔═╡ f7cc0487-5cd3-45c6-80da-af181eb30407
md"## Data Loader"

# ╔═╡ ef505909-8e93-4e27-a5e8-489ced71645f
train_data = DataLoader(x_train, y_train, batchsize = 128,  shuffle=true)

# ╔═╡ ff066d92-2217-4757-8ed3-3bb59cf1cbaa
md"## Building model"

# ╔═╡ efd5e9cf-b6c0-4ec1-a088-a25a315ebad8
model = Chain(
    # 28x28 => 14x14
    Conv((5, 5), 1=>8, pad=2, stride=2, relu),
    # 14x14 => 7x7
    Conv((3, 3), 8=>16, pad=1, stride=2, relu),
    # 7x7 => 4x4
    Conv((3, 3), 16=>32, pad=1, stride=2, relu),
    # 4x4 => 2x2
    Conv((3, 3), 32=>32, pad=1, stride=2, relu),
    
    # Average pooling on each width x height feature map
    GlobalMeanPool(),
    flatten,
    
    Dense(32, 10),
    softmax)

# ╔═╡ 8657282b-1112-4a6f-b745-1e0a75d477a2
md"## Prediction"

# ╔═╡ 9313dd38-9c30-41d5-8711-f42b19f606b1
begin
	# Getting predictions
    ŷ = model(x_train)
    # Decoding predictions
    ŷ = onecold(ŷ)
    println("Prediction of first image: $(ŷ[1])")
end

# ╔═╡ 0f420d72-6731-42b6-a3d2-0a2cc5f19f45
accuracy(ŷ, y) = mean(onecold(ŷ) .== onecold(y))

# ╔═╡ c2527b4e-c693-4cc8-a2ef-a95d53f19161
loss(x, y) = Flux.crossentropy(model(x), y)

# ╔═╡ dc899dda-1ba8-4689-9180-7cde94a04420
lr = 0.1

# ╔═╡ ef759430-584e-4419-87e0-6b3c66fde8f3
optmser = Descent(lr)

# ╔═╡ 3e9d3466-a2af-40bf-b0bd-d941a249512c
ps = Flux.params(model)

# ╔═╡ 73fade9e-75d3-4d27-b3c6-65314170f082
number_epochs = 10

# ╔═╡ 93ee948b-4fba-4058-a311-1ded3f850c29
@epochs number_epochs Flux.train!(loss, ps, train_data, opt)

# ╔═╡ 04f63535-91d3-40e5-942c-653aaa4f9124
accuracy(model(x_train), y_train)

# ╔═╡ f286069d-09ed-4dec-bfab-f448fed1f245
for batch in train_data
    
    gradient = Flux.gradient(ps) do
      # Remember that inside the loss() is the model
      # `...` syntax is for unpacking data
      training_loss = loss(batch...)
      return training_loss
    end
    
    Flux.update!(opt, ps, gradient)
end

# ╔═╡ 08cdb58b-3d0b-4c97-b930-32850ddc205f
for x in ps
    x .-= lr .* gradient[x] # Update parameters
end

# ╔═╡ ea19f028-2aa5-4105-9163-26ba6cc1dabe
loss_vector = Vector{Float64}()

# ╔═╡ 5227a424-a7e6-4412-9d4f-74e31c8c0f19
callback() = push!(loss_vector, loss(x_train, y_train))

# ╔═╡ 5b11df14-6fea-429f-9dcc-6b9f50eb8e28
Flux.train!(loss, ps, train_data, opt, cb=callback)

# ╔═╡ Cell order:
# ╠═4970a490-d1ca-11eb-09ea-d1f964b1dbe6
# ╠═99525720-3c2f-4720-afd6-8f54e882b441
# ╠═b0c701d6-9c81-4eaa-aec5-804d21748f36
# ╠═91df2c6e-87f9-4e1f-b3b6-592ff6d8dc4a
# ╠═d0bce8f3-1383-4b4e-b4f1-71c90a9344a3
# ╠═8cd5fa85-1f83-49b7-8989-efc6b3b30900
# ╠═126acf4f-4039-417a-85df-0f4c7052083b
# ╠═0b5212f1-55fd-4e80-b930-ed9e2407951e
# ╠═0a0ffcc5-4bfa-4669-9590-df7bb5edf0a8
# ╠═f7cc0487-5cd3-45c6-80da-af181eb30407
# ╠═ef505909-8e93-4e27-a5e8-489ced71645f
# ╠═ff066d92-2217-4757-8ed3-3bb59cf1cbaa
# ╠═efd5e9cf-b6c0-4ec1-a088-a25a315ebad8
# ╠═8657282b-1112-4a6f-b745-1e0a75d477a2
# ╠═9313dd38-9c30-41d5-8711-f42b19f606b1
# ╠═0f420d72-6731-42b6-a3d2-0a2cc5f19f45
# ╠═c2527b4e-c693-4cc8-a2ef-a95d53f19161
# ╠═dc899dda-1ba8-4689-9180-7cde94a04420
# ╠═ef759430-584e-4419-87e0-6b3c66fde8f3
# ╠═3e9d3466-a2af-40bf-b0bd-d941a249512c
# ╠═73fade9e-75d3-4d27-b3c6-65314170f082
# ╠═93ee948b-4fba-4058-a311-1ded3f850c29
# ╠═04f63535-91d3-40e5-942c-653aaa4f9124
# ╠═f286069d-09ed-4dec-bfab-f448fed1f245
# ╠═08cdb58b-3d0b-4c97-b930-32850ddc205f
# ╠═ea19f028-2aa5-4105-9163-26ba6cc1dabe
# ╠═5227a424-a7e6-4412-9d4f-74e31c8c0f19
# ╠═5b11df14-6fea-429f-9dcc-6b9f50eb8e28
